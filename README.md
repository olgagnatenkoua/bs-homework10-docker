# React Chat project for Binary Studio Academy - Part 2
This is a basic React chat app with predefined message list and single user (in this task).
User can add, delete, update own messages, like or unlike others' messages.

# Technologies
- React
- Redux

# How to start
Open start page: http://localhost:8082 

# Docker Hub Link
This project is available at Docker hub: https://hub.docker.com/r/olgagnatenko/react-chat