import React from "react";
import "./Message.css";

class Message extends React.Component {
  renderAvatar = ({ ownedByMe, avatar }) => {
    if (!ownedByMe) {
      return (
        <div className="message__avatar">
          <img src={avatar} alt="avatar" />
        </div>
      );
    }
  };

  renderAside = ({ created_at, likes, ownedByMe, id }, likeMessage) => {
    const displayTime = new Date(created_at + " GMT+0000").toLocaleTimeString(
      "en-US"
    );
    return (
      <div className="message__aside">
        <div className="message__time">{displayTime}</div>
        <div className="message__likes" data-message-id={id}>
          {this.renderLikes(likes)}
          {this.renderLikeBtn(ownedByMe, likeMessage)}
          {this.renderEditBtn(ownedByMe)}
          {this.renderDeleteBtn(ownedByMe)}
        </div>
      </div>
    );
  };

  renderEditBtn = ownedByMe => {
    return ownedByMe ? (
      <div className="message__edit" onClick={this.handleEditClick}>
        <i className="fas fa-edit" />
      </div>
    ) : null;
  };

  renderDeleteBtn = ownedByMe => {
    return ownedByMe ? (
      <div className="message__delete" onClick={this.handleDeleteClick}>
        <i className="fas fa-trash" />
      </div>
    ) : null;
  };

  renderLikeBtn = ownedByMe => {
    return !ownedByMe ? (
      <div className="message__like" onClick={this.handleLikeClick}>
        <i className="far fa-heart" />
      </div>
    ) : null;
  };

  renderLikes = likes => {
    return likes ? (
      <div className="message__like-count">
        <i className="far fa-grin-hearts" />
        <span>{likes}</span>
      </div>
    ) : null;
  };

  findMsgId = evt => {
    try {
      const msgLikesElement = evt.target.parentNode.parentNode;
      return msgLikesElement.getAttribute("data-message-id");
    } catch (error) {
      console.log(error);
    }
  };

  handleLikeClick = evt => {
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.likeMessage(id);
  };

  handleEditClick = evt => {
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.editMessage(id);
  };

  handleDeleteClick = evt => {
    const id = this.findMsgId(evt);
    if (!id) return;
    this.props.handlers.deleteMessage(id);
  };

  render = () => {
    const { message, handlers } = this.props;
    const { likeMessage, deleteMessage, editMessage } = handlers;
    const { ownedByMe } = message;

    const ownMessageClassName = ownedByMe ? " own-message" : "";
    const messageClassName = `message ${ownMessageClassName}`;
    return (
      <article className={messageClassName}>
        <div className="message__body">
          {this.renderAvatar(message)}
          <div className="message__text">{message.message}</div>
        </div>
        {this.renderAside(message, likeMessage)}
      </article>
    );
  };
}

export default Message;
