import { createStore } from "redux";
import initialState from "./helpers/initialState"

import reducer from "./components/Chat/reducer";
export default createStore(
  reducer,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
