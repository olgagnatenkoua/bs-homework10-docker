import React from "react";
import "./App.css";
import Chat from "./components/Chat/Chat";

function App() {
  return (
    <div className="App container">
      <header className="header">
        <div className="logo">
          <img src="img/logo.jpg" alt="logo" className="logo__img" />
          <span className="logo__text">YET ANOTHER CHAT :)</span>
        </div>
      </header>
      <Chat />
      <footer className="footer">
        <div className="copyright">
          <span>COPYRIGHT</span>
        </div>
      </footer>
    </div>
  );
}

export default App;
