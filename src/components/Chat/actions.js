import {
  COMPLETE_FETCH_MESSAGES,
  LIKE_MESSAGE,
  START_EDIT_MESSAGE,
  DELETE_MESSAGE,
  ADD_MESSAGE,
  COMPLETE_EDIT_MESSAGE,
  CANCEL_EDIT_MESSAGE
} from "./actionTypes";

export const likeMessage = id => ({
  type: LIKE_MESSAGE,
  payload: {
    id
  }
});

export const startEditMessage = message => ({
  type: START_EDIT_MESSAGE,
  payload: {
    message
  }
});

export const completeEditMessage = message => ({
  type: COMPLETE_EDIT_MESSAGE,
  payload: {
    message
  }
});

export const cancelEditMessage = () => ({
  type: CANCEL_EDIT_MESSAGE,
  payload: {}
});

export const addMessage = message => ({
  type: ADD_MESSAGE,
  payload: {
    message
  }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: {
    id
  }
});

export const fetchMessages = messages => ({
  type: COMPLETE_FETCH_MESSAGES,
  payload: {
    messages
  }
});
