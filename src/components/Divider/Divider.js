import React from "react";
import "./Divider.css";

class Divider extends React.Component {
  render = () => <div className="divider">{this.props.text}</div>;
}

export default Divider;
