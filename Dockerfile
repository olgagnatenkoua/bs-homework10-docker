FROM node:current-alpine AS build

COPY package*.json /
RUN npm install

COPY public /public
COPY src /src

CMD ["npm", "run", "start"]

EXPOSE 3000 



